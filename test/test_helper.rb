require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  fixtures :all

  def file_100_100_png(filename = '100x100.png')
    build_file(filename)
  end

  def build_file(filename)
    File.new("test/fixtures/files/#{filename}")
  end
end
