require 'test_helper'

class PlaceTest < ActiveSupport::TestCase
  test 'should shutdown without some args' do
    place = Place.new
    assert_not place.save, '-> Place saved without args!'

    place = Place.new(y: 0, project_id: 12)
    place.image = file_100_100_png
    assert_not place.save, '-> Place saved without x!'

    place = Place.new(x: 0, project_id: 12)
    place.image = file_100_100_png
    assert_not place.save, '-> Place saved without y!'

    place = Place.new(x: 0, y: 12)
    place.image = file_100_100_png
    assert_not place.save, '-> Place saved without ic!'

    place = Place.new(x: 0, y: 12, project_id: 13)
    assert_not place.save, '-> Place saved without image!'
  end

  test 'should save Place with x, y, project, image' do
    place = Project.take.places.new(x: 0, y: 12)
    place.image = file_100_100_png

    assert place.save, '-> Place not saved!'
  end
end

# == Schema Information
#
# Table name: places
#
#  id                 :integer          not null, primary key
#  project_id         :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  description        :text
#  x                  :integer
#  y                  :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  title              :string
#
