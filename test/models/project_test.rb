require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test 'check validates attributes for Project' do
    project = Project.new
    assert_not project.save, '-> Project saved without args!'

    project = Project.new(place_width: 20, place_height: 20)
    project.image = build_file('1600x1058.jpg')
    assert_not     project.save,   '-> Project saved without title!'
  end

  test 'should dimension create' do
    project = Project.new(title: 'test', place_width: 20, place_height: 20, width: 1500, height: 1000)
    project.image = build_file('1600x1058.jpg')
    project.save
    assert_not_nil project.height, '-> Project height is nil!'
    assert_not_nil project.width,  '-> Project width is nil!'
    assert_equal   project.height, 1000, '-> Project height not equal 1000!'
    assert_equal   project.width,  1500, '-> Project width not equal 1500!'
  end

   test 'should html generate' do
     project = Project.new(title: 'test', place_width: 20, place_height: 20, width: 1500, height: 1000)
     project.image = build_file('1600x1058.jpg')
     project.save
     assert_not_nil project.html,   '-> Project html is nil!'
   end

   test 'should translite FriendlyId' do
     project = Project.new(title: 'Test', place_width: 20, place_height: 20, width: 1500, height: 1000)
     project.image = build_file('1600x1058.jpg')
     project.save
     assert_equal project.slug, 'test', '-> Fail slug Test'

     project = Project.new(title: 'Тесто', place_width: 20, place_height: 20, width: 1500, height: 1000)
     project.image = build_file('1600x1058.jpg')
     project.save
     assert_equal project.slug, 'testo', '-> Fail slug Тесто'
   end

   test 'should standart dimension after create' do
     project = Project.new(
       title: 'Test',
       width: 1500,
       height: 1000,
       place_height: 20,
       place_width: 20
     )
     project.image = build_file('1600x1058.jpg')

     assert project.save, '-> Project not saved!'
     assert FastImage.size(project.image.path)[0] <= project.width, '-> Project width great then max_width'
     assert FastImage.size(project.image.path)[1] <= project.height, '-> Project width great then max_height'
   end
end

# == Schema Information
#
# Table name: projects
#
#  id                 :integer          not null, primary key
#  title              :string
#  html               :binary
#  height             :integer
#  width              :integer
#  place_height       :integer
#  place_width        :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  places_in_y        :integer
#  places_in_x        :integer
#  slug               :string
#  description        :text
#  download_count     :integer          default(0)
#  places_count       :integer          default(0)
#
