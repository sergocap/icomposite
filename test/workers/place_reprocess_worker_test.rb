require 'test_helper'
require 'sidekiq/testing'

class PlaceReprocessWorkerTest < ActiveSupport::TestCase
  include SidekiqMinitestSupport

  test 'after create place should worker started' do
    place = Project.take.places.new(
                                    x: 0, y: 12,
                                    r_component: 100, g_component: 50, b_component: 20,
                                    height: 600, width: 800,
                                    crop_x: 10, crop_y: 20,
                                    crop_height: 200, crop_width: 240
                                    )
    place.image = build_file('1600x1200.jpg')
    assert_equal 0, PlaceReprocessWorker.jobs.size, '-> Count works for reprocess place not a zero!'
    place.save
    assert_equal 1, PlaceReprocessWorker.jobs.size, '-> Worker not started!'
  end
end
