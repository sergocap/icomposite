class AddPlacesCountToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :places_count, :integer, default: 0
  end
end
