class AddTitleToPlace < ActiveRecord::Migration[5.1]
  def change
    add_column :places, :title, :string
  end
end
