class AddCountPlacesToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :places_in_y, :integer
    add_column :projects, :places_in_x, :integer
  end
end
