class CreatePlaces < ActiveRecord::Migration[5.1]
  def change
    create_table :places do |t|
      t.belongs_to  :project
      t.attachment  :image
      t.text        :description
      t.integer     :x, :y

      t.timestamps
    end
  end
end
