class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string      :title
      t.binary      :html
      t.integer     :height, :width, :place_height, :place_width
      t.attachment  :image

      t.timestamps
    end
  end
end
