class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.string :state
      t.text :text
      t.belongs_to :user

      t.timestamps
    end
  end
end
