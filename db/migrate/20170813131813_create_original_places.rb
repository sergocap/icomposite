class CreateOriginalPlaces < ActiveRecord::Migration[5.1]
  def change
    create_table :original_places do |t|
      t.integer :x, :y
      t.belongs_to :project
      t.attachment :image

      t.timestamps
    end
  end
end
