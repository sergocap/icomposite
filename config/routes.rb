Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'my_devise/registrations',
    sessions: 'my_devise/sessions',
    omniauth_callbacks: 'my_devise/omniauth_callbacks'
  }

  devise_scope :user do
    get 'change_password', to: 'my_devise/registrations#change_password'
    post 'change_password', to: 'my_devise/registrations#update_password'
  end

  root 'projects#index'

  get 'places_navigation', to: 'places#navigation'

  resources :reviews, only: [:index, :create]

  namespace :manage do
    root 'projects#index'
    resources :projects
  end

  resources :projects, only: [:show] do
    get 'download', on: :member
    get 'download_counter', on: :member
    resources :places
  end
end
