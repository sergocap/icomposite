class ReviewsController < ApplicationController
  load_and_authorize_resource

  def index
    @reviews = Review.published.order('created_at desc').page(params[:page]).per(Review.per_page)

    unless request.xhr?
      @review = Review.new
    else
      render partial: 'reviews_page', locals: { reviews: @reviews } and return
    end
  end

  def create
    @review = current_user.reviews.new(review_params)
    if @review.save
      redirect_to reviews_path, notice: 'Благодарим за вклад в развитие проекта!
        Ваше сообщение будет обработано и опубликовано в скором времени.'
    else
      redirect_to reviews_path, alert: 'Что-то пошло не так :('
    end
  end

  private
  def review_params
    params.require(:review).permit([:text])
  end
end
