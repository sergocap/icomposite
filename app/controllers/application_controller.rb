class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :resolve_layout

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => 'Не хватает прав для выполнения этого действия'
  end

  private
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:avatar, :name, :crop_x, :crop_y, :crop_height, :crop_width])
    devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :name, :crop_x, :crop_y, :crop_height, :crop_width])
  end

  def resolve_layout
    devise_controller? ? 'col-sm-5' : 'application'
  end

  def current_ability
    Ability.new(current_user, controller_namespace)
  end

  def controller_namespace
    controller_name_segments = params[:controller].split('/')
    controller_name_segments.pop
    @controller_namespace = controller_name_segments.join('/').camelize
  end
end
