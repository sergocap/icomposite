class ProjectsController < ApplicationController
  load_and_authorize_resource
  before_action :find_projects, only: [:show]

  def index
    raise 'Нет проектов' if Project.none?
    redirect_to project_path(Project.select('slug').first.slug)
  end

  def download_counter
    @project.download_count += 1
    @project.save
    render json: {} and return
  end

  def download
    layout_var = request.xhr? ? 'modal1' : 'col-sm-5'
    render :download, layout: layout_var
  end

  private
  def find_projects
    @projects = Project.all
  end
end
