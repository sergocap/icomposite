class MyDevise::RegistrationsController < Devise::RegistrationsController
  after_action :avatar_reprocess, only: [:update]
  prepend_before_action :authenticate_scope!, only: [:change_password, :update_password, :edit, :update, :destroy]
  before_action :check_provider, only: [:change_password, :update_password]

  def update_password
    current_user.errors.add :current_password, :invalid unless current_user.valid_password? params['user']['current_password']

    if current_user.errors.none? && current_user.update(pass_params)
      bypass_sign_in current_user
      flash[:notice] = 'Пароль успешно изменён'
      redirect_to edit_user_registration_path
    else
      render :change_password
    end
  end

  private
  def after_update_path_for(resource)
    edit_user_registration_path
  end

  def check_provider
    redirect_to root_path, notice: 'Пользователи, вошедшие с помощью соц. сетей, не нуждаются в пароле' if current_user.with_provider?
  end

  def avatar_reprocess
    resource.avatar_reprocess! if resource.errors.none?
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def pass_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
