class MyDevise::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def self.generate_callback_for(provider)
    class_eval do
      define_method(provider) do
        @user = User.send "find_for_#{provider}_oauth", request.env["omniauth.auth"]
        if @user.persisted?
          flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => @user.provider_name
          sign_in_and_redirect @user, :event => :authentication
        else
          flash[:notice] = 'Ошибка авторизациии'
          redirect_to root_path
        end

      end
    end
  end

  User.omniauth_providers.each do |provider|
    generate_callback_for provider
  end
end
