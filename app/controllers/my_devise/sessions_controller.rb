class MyDevise::SessionsController < Devise::SessionsController
  def new
    super do
      if request.xhr?
        render :new, layout: 'modal1' and return
      end
    end
  end
end
