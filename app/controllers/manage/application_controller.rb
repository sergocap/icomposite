class Manage::ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout 'manage'

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => 'Не хватает прав для выполнения этого действия'
  end

  private
  def current_ability
    Ability.new(current_user, controller_namespace)
  end

  def controller_namespace
    controller_name_segments = params[:controller].split('/')
    controller_name_segments.pop
    @controller_namespace = controller_name_segments.join('/').camelize
  end
end
