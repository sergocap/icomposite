class Manage::ProjectsController < Manage::ApplicationController
  load_and_authorize_resource

  def create
    if @project.save
      redirect_to manage_project_path(@project)
    else
      render :new
    end
  end

  def update
    if @project.update_attributes(project_params)
      redirect_to manage_project_path(@project)
    else
      render :edit
    end
  end

  def destroy
    @project.destroy
    redirect_to manage_projects_path
  end

  private
  def find_projects
    @projects = Project.all
  end

  def project_params
    params.require(:project).permit([:title, :description,
                                     :width, :height,
                                     :place_height, :place_width,
                                     :image])
  end
end
