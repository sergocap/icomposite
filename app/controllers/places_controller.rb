class PlacesController < ApplicationController
  load_and_authorize_resource
  before_action :find_project, only: [:new, :create]
  before_action :find_place, only: [:show, :navigation]
  layout 'modal1'

  def show
    render 'show', layout: 'modal2'
  end

  def navigation
    @place = Place.send("#{params['target']}_redis_place", @place)
    render 'show', layout: false
  end

  def new
    if request.xhr?
      @original_place = @project.original_places.find_or_create_by(x: params[:x], y: params[:y])
      @place = Place.new(x: params[:x], y: params[:y])
    else
      redirect_to '/404'
    end
  end

  def create
    @place = @project.places.new(place_params)
    if @place.save
      render json: { close_modal: true } and return
    else
      @original_place = @project.original_places.find_by(x: @place.x, y: @place.y)
      render :new, layout: false
    end
  end

  private
  def place_params
    params.require(:place).permit([:crop_x, :crop_y,
                                   :crop_height, :crop_width,
                                   :r_component, :g_component, :b_component, :a_component,
                                   :title, :description, :image,
                                   :y, :x])
  end

  def find_project
    @project = Project.find(params[:project_id])
  end

  def find_place
    @place = Place.redis_place(params[:project_id] || params[:project_slug], params[:x], params[:y])

    unless @place.present?
      @place = Place.find(params[:id])
      @place.write_redis_infos!
    end
  end
end
