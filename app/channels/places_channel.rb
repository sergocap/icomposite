class PlacesChannel < ApplicationCable::Channel
  def subscribed
    stream_from "places_for_project_#{params[:project_slug]}"
  end

  def unsubscribed
  end
end
