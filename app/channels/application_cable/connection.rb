module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :uuid_key

    def connect
      self.uuid_key = SecureRandom.uuid
    end
  end
end
