function init_auto_height() {
  $('#content').css('min-height', 0);
  $('#content').css('min-height',
    $(document).height() - $('#header').height() - $('#footer').height()
  );
  $('#footer').css('visibility', 'visible');
}

function init_menu() {
  limit_middle = 700;
  nav = $('#header_menu nav');
  if (nav.hasClass('large') && $(window).width() <= limit_middle) {
    menu = nav.find('.true_menu');
    new_menu_html =
      "<nav class='small'>" +
        "<ul class='nested_menu'>" +
          "<li>" +
            "<a class='no-after child-list-link' href='javascript:void(0)'>" +
              "<i class='fa fa-bars'></i>" +
            "</a>" +
            "<ul class='child-list true_menu'>" +
              menu.html() +
            "</ul>" +
          "</li>" +
        "</ul>" +
      "</nav>";
    nav.remove();
    $('#header_menu').append(new_menu_html);
    nav = $('#header_menu nav');
  }
  if (nav.hasClass('small') && $(window).width() > limit_middle) {
    menu = nav.find('.true_menu');
    new_menu_html =
      "<nav class='large'>" +
          "<ul class='nested_menu true_menu'>" +
            menu.html() +
          "</ul>" +
      "</nav>";
    nav.remove();
    $('#header_menu').append(new_menu_html);
    nav = $('#header_menu nav');
  }
  nav.css('visibility', 'visible')
}

function init_image_with_cropper() {
  input = $('.js-image_with_cropper');
  field_wrapper = input.closest('.form-group');
  form = input.closest('form');

  button_html = "<p><button type='button' class='button button_blue button_long load_image'>" + input.data('button-title') + "</button><p>"
  preview_wrapper_html = "<div class='hidden preview_wrapper' style='position:relative;'>" +
                           "<img class='preview'" +
                             "style='max-width:"+input.data('max-width')+"px; " +
                             "max-height:"+input.data('max-height')+"px'></img>" +
                         "</div>";

  input.after(preview_wrapper_html);
  input.after(button_html);

  preview_wrapper = $('.preview_wrapper', field_wrapper);
  preview = $('img.preview', preview_wrapper);
  button = $('.button.load_image', form);

  button.on('click', function() {
    input.click()
  });

  input.on('change', function(e) {
    file = input.prop('files')[0];
    if (!file) {
      preview.cropper('destroy');
      preview.attr('src', '');
      $('input[id*="crop_"]', form).val('');
    }
    else if(file.type.match('image/jpeg') || file.type.match('image/png')) {
      reader = new FileReader();

      reader.onload = function(e){
        src = e.target.result;
        preview_wrapper.removeClass('hidden');

        preview.attr('src', src).on('load', function(){
          preview.cropper('destroy');
          init_cropper(form, preview, input.data());
        })

      }
      reader.readAsDataURL(file);
    }
    else alert('Выберите картинку. (jpeg, jpg, png)');
  });
}

function init_cropper(form, image, data){
  crop_x_input =      $('input[id*="crop_x"]', form);
  crop_y_input =      $('input[id*="crop_y"]', form);
  crop_width_input =  $('input[id*="crop_width"]', form);
  crop_height_input = $('input[id*="crop_height"]', form);

  image.cropper({
    aspectRatio: data.aspectRatio,
    viewMode: 3,
    dragMode: 'move',
    guides: false,
    minCropBoxWidth: 200,
    minCropBoxHeight: 200,
    crop: function(e) {
      crop_x_input.val(e.x);
      crop_y_input.val(e.y);
      crop_width_input.val(e.width);
      crop_height_input.val(e.height);
    }
  });
}

function init_jssocials_site() {
  $('.jssocials_site').jsSocials({
    text: $(this).data('text'),
    url: $(this).data('url'),
    shareIn: 'popup',
    showCount: false,
    showLabel: false,
    shares: ['vkontakte', 'twitter', 'facebook', 'googleplus']
  })
}
