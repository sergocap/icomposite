$(document).ready(function(){
  if($('.js-remote_modal, .ajax-modal1_load, ajax-place_show').length) init_modals();
  if($('.channel-places').length) init_places_channel();
  if($('.js-project_preview_with_net').length) init_project_preview_with_net();
  if($('.js-table').length) init_adaptive_table();
  if($('.js-image_with_cropper').length) init_image_with_cropper();
  if($('.jssocials_project').length) init_jssocials_project();
  if($('.jssocials_site').length) init_jssocials_site();
  if($('.js-load_more_reviews').length) init_load_more_reviews();
  if($('.js-download_counter, #project_show ').length) init_download_counter();
  init_auto_height();
  init_menu();

  $(".alert").delay(5000).fadeTo(500, 0);
});

$(window).on('resize', function() {
  if($('.js-table').length) init_adaptive_table();
  init_auto_height();
  init_menu();
});

