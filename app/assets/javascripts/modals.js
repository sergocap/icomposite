function init_modals(){
  modal_holder_selector = '#modal_holder';
  modal_selector1 = '#modal1';
  modal_selector2 = '#modal2';

  $(document).on('ajax:success', '.ajax-modal1_load', function(event, data) {
    $(modal_holder_selector).html(data).find(modal_selector1).modal('show');
    if($('.js-place_preview').length) init_place_preview();
  });

  $(document).on('ajax:success', '.js-remote_modal', function(event, data) {
    $(modal_holder_selector).html(data).find(modal_selector1).modal('show');
  });

  $(document).on('ajax:success', '.ajax-place_show', function(event, data) {
    $(modal_holder_selector).html(data).find(modal_selector2).modal('show');
  });

  $(document).on('click', '.close_modals', function() {
    remove_modals();
  });

  $(document).on('click', '.ajax-submit_modal_form', function() {
    form = $(this).closest('.simple_form');
    if(form.data('waiting') != 'ajax')
    {
      form.data('waiting', 'ajax');
      $(this).addClass('disabled_element');
      $(form).ajaxSubmit({
        success: function(data){
          if(data.message) alert(data.message);
          if(data.close_modal)
            remove_modals();
          else {
            $(modal_selector1).find('.modal-content').html(data);
            if($('.js-place_preview').length) init_place_preview();
          }
          form.data('waiting', 'none');
        }
      });
    }
  });

  $(document).on('click', '.place_show .nav-places', function() {
    nav = $(this);
    datas = nav.closest('.navigation').data();
    $.ajax({
      url: '/places_navigation',
      method: 'GET',
      data: {
        target: (nav.hasClass('nav-next') ? 'next' : 'previous'),
        project_slug: datas.projectSlug,
        x: datas.x,
        y: datas.y,
        id: datas.id
      },
      success: function(res) {
        $(modal_selector2).find('.modal-content').html(res);
      },
      error: function() {
        alert('Что-то пошло не так :(')
      }
    })
  });

  $(document).on("keypress", '#place_new_modal form input', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        $('.ajax-submit_modal_form', $(this).closest('form')).click();
        return false;
    }
  });
}

function remove_modals() {
  $('.modal-backdrop').remove();
  $('.modal').remove();
  $('body').removeClass( "modal-open" );
  $('body').css('padding', '0')
}

