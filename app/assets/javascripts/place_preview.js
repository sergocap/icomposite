function init_place_preview(){
  // элементы идут вот так
  // input[type='file'], button.load_image,
  // .preview_wrapper,
  // .cropp_mini_preview
  // .minicolors_wrapper
  //

  input_image = $('.js-place_preview');
  form = input_image.closest('form');
  input_image.addClass('hidden');

  button = "<button type='button' class='button button_blue button_long load_image'>Выбрать изображение</button>"
  preview_wrapper = "<div class='preview_wrapper' style='min-height:200px;position:relative; margin-top:15px'>" +
              "<img class='preview'" +
                "style='max-width:"+input_image.data('max-width')+"px; " +
                "max-height:"+input_image.data('max-height')+"px'></img>" +
            "</div>";

  mini_preview_wrapper = "<div class='mini_previews_wrapper'><div class='cropp_mini_preview'" +
              "style='display:inline-block; vertical-align:middle; height:"+input_image.data('mini-preview-height')+"px; " +
                      "width: "+input_image.data('mini-preview-width')+"px; " +
                      "overflow:hidden'></div></div>"

  minicolors_wrapper = $('.minicolors_wrapper', form);

  input_image.after(preview_wrapper);
  input_image.after(button);

  button = $('.load_image', form);

  preview = $('img.preview', form);
  preview_wrapper = $('.preview_wrapper', form);
  preview_wrapper.after(mini_preview_wrapper);
  cropp_mini_preview = $('.cropp_mini_preview', form);

  field_wrapper = input_image.closest('.form-group');

  original_place = $('.original_place', form);
  original_place.css({ 'height': input_image.data('mini-preview-height') + 'px',
    'width': input_image.data('mini-preview-width') + 'px', 'vertical-align': 'middle' })

  cropp_mini_preview.before(original_place);

  delimiter = $('.glyphicon.delimiter', form);
  original_place.after(delimiter);
  delimiter.css('vertical-align', 'middle');

  error_span = $('span.help-block', field_wrapper);
  button.after(error_span);

  button.on('click', function(){
    input_image.click();
  });

  input_image.on('change', function(e){
    file = input_image.prop('files')[0];
    if (!file) {
      preview.cropper('destroy');
      field_wrapper.find('.help-block').removeClass('hidden');
      preview.attr('src', '')
    }
    else if(file.type.split('/')[0] == 'image'){
      reader = new FileReader();

      reader.onload = function(e){
        src = e.target.result;
        field_wrapper.find('.help-block').addClass('hidden');

        preview_wrapper.css('visibility', 'hidden');
        preview_wrapper.append('<div class="loading_div"></div>');
        preview.attr('src', src).on('load', function(){
          preview.cropper('destroy');
          init_cropp(preview, input_image.data());
          set_rgba_inputs(255,255,255,1);
          setTimeout(function() {
            preview_wrapper.find('.loading_div').remove();
            preview_wrapper.css('visibility', 'visible');
          }, 2500);
        })

      }
      reader.readAsDataURL(file);
    }
    else alert('Выберите картинку!');
  });

  minicolors_input = $('input#minicolors_input', form);
  minicolors_input.minicolors({
    format: 'rgb',
    inline: false,
    opacity: true,
    defaultValue: 'rgba(200,200,200,1)',
    position: 'bottom right',
    theme: 'bootstrap',
      change: function(rgba){
        array_res = /^rgba\(([0-9]+), ([0-9]+), ([0-9]+), ([\.0-9]+)\)/.exec(rgba);
        set_rgba_inputs(array_res[1], array_res[2], array_res[3], array_res[4]);

        $('.cropper-crop-box .cropper-face', form).css({ opacity: '1', backgroundColor: rgba, mixBlendMode: 'multiply' });
        $('.cropp_mini_preview', form).css({ backgroundColor: rgba });
        $('.cropp_mini_preview img', form).css({ mixBlendMode: 'multiply' });
      }
  });

  function set_rgba_inputs(r, g, b, a){
    $('#place_r_component', form).val(r);
    $('#place_g_component', form).val(g);
    $('#place_b_component', form).val(b);
    $('#place_a_component', form).val(a);
  }

  function init_cropp(image, data){
    crop_x_input =      $('#place_crop_x', form);
    crop_y_input =      $('#place_crop_y', form);
    crop_width_input =  $('#place_crop_width', form);
    crop_height_input = $('#place_crop_height', form);

    image.cropper({
      aspectRatio: data.aspectRatio,
      viewMode: 3,
      dragMode: 'move',
      guides: false,
      minCropBoxWidth: 200,
      minCropBoxHeight: 200,
      preview: '.cropp_mini_preview',
      crop: function(e) {
        crop_x_input.val(e.x);
        crop_y_input.val(e.y);
        crop_width_input.val(e.width);
        crop_height_input.val(e.height);
      }
    });

    $('.cropper-container .cropper-modal', form).css('opacity', '0.8');
    $('.cropper-crop-box .cropper-face', form).css({ opacity: '1', backgroundColor: 'rgba(255,255,255,1)', mixBlendMode: 'multiply' });
  }
}
