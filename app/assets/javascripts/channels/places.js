function init_places_channel() {
  channel_places = $('.channel-places');
  if (channel_places.data('connected') == 'true')
    return 0;

  project_slug = channel_places.data('project-slug');
  App.places = App.cable.subscriptions.create({ channel: 'PlacesChannel', project_slug: project_slug }, {

    connected: function() {
      console.log('stream for channel places_for_project_'+project_slug);
      channel_places.data('connected', 'true');
    },

    disconnected: function() {
    },

    received: function(data) {
      if(data.action == 'new') {
        add_place_to_project(data.place);
        $('#title_and_description .complete_info, .project_item.active .complete_info').html(data.complete_info)
      }
    }
  });

  function add_place_to_project(place) {
    class_for_show = 'ajax-place_show'

    target_a_place = $('.js-table ' +
            '.row_project:nth-child('+(parseInt(place.y)+1)+') '+
            '.column_project:nth-child('+(parseInt(place.x)+1)+') a')

    target_a_place.css({ background: 'url('+place.image_url+')', backgroundSize: 'contain' });
    target_a_place.attr('class', class_for_show);
    target_a_place.attr('href', place.url);
  }
}
