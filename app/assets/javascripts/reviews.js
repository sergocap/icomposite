function init_load_more_reviews() {
  that = $('.js-load_more_reviews');
  reviews_wrapper = $('.reviews');
  total_count = that.data().totalCount;

  that.on('click', function() {
    $.ajax({
      data: {
        page: that.data().nextPage
      },
      success: function(res) {
        reviews_wrapper.prepend(res);
        current_count = $('.review', reviews_wrapper).length;
        if(current_count == total_count) {
          that.remove();
        } else {
          that.data().nextPage = that.data().nextPage + 1;
        }
      },
      error: function(res) {
        console.log(res);
      }
    })
  })
}
