function init_adaptive_table() {
  project_wrapper = $('.js-project_wrapper');
  table = $('.js-table', project_wrapper);

  table_width_old = table.width();
  table_height_old = table.height();

  rel = project_wrapper.width() * 1.0 / table_width_old;

  table_height_new = table_height_old * rel;
  table_width_new = table_width_old * rel;

  table.css('transform', 'scale(' + rel + ',' + rel + ')');
  table.css('margin-left', (table_width_new - table_width_old)/2.0 + 'px');
  table.css('margin-top', (table_height_new - table_height_old)/2.0 + 'px');
  project_wrapper.height(table_height_new + 'px');
  table.css('visibility', 'visible');
}

function init_jssocials_project() {
  $('.jssocials_project').jsSocials({
    text: $(this).data('text'),
    url: $(this).data('url'),
    shareIn: 'popup',
    shares: ['vkontakte', 'twitter', 'facebook', 'googleplus']
  })
}

function init_download_counter() {
  $(document).on('click', '.js-download_counter', function() {
    $.get($(this).data().url)
  })
}
