function init_project_preview_with_net(){
  input_image = $('.js-project_preview_with_net');
  input_image.addClass('hidden');

  button = "<button type='button' class='button button_blue button_long load_image'>Выбрать изображение</button>";
  preview_wrapper = "<div class='preview_wrapper hidden' style='position:relative; margin-top:15px; overflow:overlay'>" +
              "<img class='preview'" +
                "style='max-width:"+input_image.data('max-width')+"px; " +
                "max-height:"+input_image.data('max-height')+"px'></img>" +
              "<canvas style='position:absolute; top:0; left:0' width='0' height='0' class='canvas_net'></canvas>" +
            "</div>";

  input_image.after(preview_wrapper);
  input_image.after(button);

  place_width = $('#project_place_width');
  place_height = $('#project_place_height');

  input_width = $('#project_width');
  input_height = $('#project_height');

  preview_wrapper = $('.preview_wrapper');

  preview = $('img.preview');
  canvas_net = $('canvas.canvas_net');

  $('.load_image').on('click', function(){
    input_image.click();
  });

  input_image.on('change', function(e){
    file = input_image.prop('files')[0];

    if(file.type.split('/')[0] == 'image'){
      reader = new FileReader();

      reader.onload = function(e){
        src = e.target.result;
        preview_wrapper.removeClass('hidden');

        preview.attr('src', src).on('load', function(){
          input_width.val(this.width);
          input_height.val(this.height);

          draw_canvas_net(canvas_net, this.width, this.height, place_width.val(), place_height.val())
        })

      }
      reader.readAsDataURL(file);
    }
    else alert('Выберите картинку!');
  });

  $('.js-place_size_apply').on('click', function(){
    draw_canvas_net(canvas_net, preview.width(), preview.height(), place_width.val(), place_height.val())
  });

  function draw_canvas_net(canvas_net, width, height, place_width, place_height){
    canvas_net.attr({ width: width, height: height });
    canvas_context = canvas_net[0].getContext('2d');

    place_width = parseInt(place_width);
    place_height = parseInt(place_height);

    canvas_context.clearRect(0, 0, width, height);

    if(place_width && place_height){
      j = 0;
      while(j <= width){
        draw_line(canvas_context, j, 0, j, height);
        j += place_width;
      }

      i = 0;
      while(i <= height){
        draw_line(canvas_context, 0, i, width, i);
        i += place_height;
      }
    }
  }

  function draw_line(context, x1, y1, x2, y2){
    context.beginPath();

    context.moveTo(x1, y1);
    context.lineTo(x2, y2);

    context.stroke();
  }
}
