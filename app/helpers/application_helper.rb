module ApplicationHelper
  def controller_action
    @controller_action ||= [controller.class.name.remove('Controller').downcase, controller.action_name].join('_')
  end

  def og_properties_meta_tags
    case controller_action
    when 'projects_show'
      [
        og_prop('title', "#@project - #{Settings.app.name}"),
        og_prop('type', 'article'),
        og_prop('image', @project.image.url(:medium)),
        og_prop('description', @project.description.truncate(100))
      ].join
    else
      [
        og_prop('title', Settings.app.name),
        og_prop('type', 'article'),
        og_prop('image', asset_path('logotype.png')),
        og_prop('description', I18n.t('app.og_description'))
      ].join
    end.html_safe
  end

  def title_text
    case controller_action
    when 'projects_show'
      "#@project - #{Settings.app.name}"
    else
      Settings.app.name
    end
  end

  def og_prop(name, into)
    content_tag(:meta, nil, property: "og:#{name}", content: into)
  end
end
