class OriginalPlace < ApplicationRecord
  belongs_to :project
  has_attached_file :image, default_url: '/images/missing.png'
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  after_create :create_image

  def create_image
    img = Magick::Image.read(project.image.path)[0]
    w = project.place_width
    x1 = w * x
    h = project.place_height
    y1 = h * y
    img.crop!(x1, y1, w, h, true)
    file = Tempfile.new(['image', '.png'])
    img.write(file.path)
    update_attribute(:image, file)
    file.close
    file.unlink
  end
end

# == Schema Information
#
# Table name: original_places
#
#  id                 :integer          not null, primary key
#  x                  :integer
#  y                  :integer
#  project_id         :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
