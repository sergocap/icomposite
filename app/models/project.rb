class Project < ApplicationRecord
  has_many :places, dependent: :destroy
  has_many :original_places, dependent: :destroy
  has_attached_file :image,
    default_url: '/images/missing.png',
    styles: { :small => '100x100^', :medium => "400x200#",  original: { processors: [:project_image_processor] } }

  validates :title, :place_height, :place_width, :width, :height, presence: true
  validates :image, attachment_presence: true
  validates_attachment_content_type :image,
    content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  include Zlib
  include ProjectObserving
  include CommonMethods
  include Rails.application.routes.url_helpers

  class << self
    attr_reader :max_width, :max_height
  end
  @max_width, @max_height = 1600, 1300

  def generate_empty_table_html
    view = get_action_view
    html_string = view.render partial: 'manage/projects/empty_table',
      layout: false, locals: { project: self }

    write_deflated_html html_string
  end

  def inflated_html
    Inflate.inflate(html)
  end

  def normalize_friendly_id(input)
   input.to_s.to_slug.normalize(transliterations: :russian).to_s
  end

  # work on sidekiq
  def compile_with_place(place)
    project_img = rmagick_image
    place_img = place.rmagick_image

    place_img.resize!(place_width, place_height, Magick::LanczosFilter, 1.0)
    project_img.composite!(place_img, place_width * place.x, place_height * place.y, Magick::OverCompositeOp)

    project_img.write(image.path)
    image.reprocess! # (:small) full image decreases in size

    html_compile_with_place(place)
  end

  def to_s
    title
  end

  def html_compile_with_place(place)
    doc = Nokogiri::HTML(inflated_html)

    link = doc.css(".js-table .row_project:nth-child(#{place.y + 1}) .column_project:nth-child(#{place.x + 1}) a")[0]
    link['class'] = 'ajax-place_show'
    link['href'] = project_place_path(self.slug, place.id, x: place.x, y: place.y)

    write_deflated_html(doc.to_html)
  end

  def total_places_count
    places_in_x * places_in_y
  end

  private
  def write_deflated_html(html_string)
    update_attribute(:html, Deflate.deflate(html_string))
  end

  def rmagick_image
    @rmagick_image ||= Magick::Image.read(image.path)[0]
  end
end

# has a project_observer!

# == Schema Information
#
# Table name: projects
#
#  id                 :integer          not null, primary key
#  title              :string
#  html               :binary
#  height             :integer
#  width              :integer
#  place_height       :integer
#  place_width        :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  places_in_y        :integer
#  places_in_x        :integer
#  slug               :string
#  description        :text
#  download_count     :integer          default(0)
#  places_count       :integer          default(0)
#
