module CommonMethods
  extend ActiveSupport::Concern

  def get_action_view
    return @action_view unless @action_view.nil?
    @action_view = ActionView::Base.new(Rails.configuration.paths["app/views"])
    @action_view.tap {|tapped|
      tapped.class_eval do
        include Rails.application.routes.url_helpers
      end
    }
  end
end
