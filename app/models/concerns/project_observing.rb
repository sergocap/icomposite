module ProjectObserving
  extend ActiveSupport::Concern

  included do
    after_create :after_create_observe
  end

  def after_create_observe
    image.reprocess!
    generate_empty_table_html
  end
end
