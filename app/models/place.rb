class Place < ApplicationRecord
  belongs_to          :project, counter_cache: true
  has_attached_file   :image, default_url: '/images/missing.png',
    styles: { original: { processors: [:place_image_processor] } }

  validates           :x, :y, :project, presence: true
  validates           :image, attachment_presence: true
  validate :x_y_for_project_uniq
  validates_attachment_content_type :image,
    content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  include Rails.application.routes.url_helpers
  include PlaceRedisObserver

  after_commit  :start_worker, on: :create
  after_create  :end_worker

  delegate :slug, to: :project, prefix: true

  class << self
    attr_reader :max_height, :max_width
  end
  @max_height, @max_width = 120, 150

  attr_accessor :crop_x, :crop_y, :crop_height, :crop_width,
    :r_component, :g_component, :b_component, :a_component

  def reprocess_image_perform(cropp_options, rgba_options)
    self.crop_width, self.crop_height, self.crop_x, self.crop_y = cropp_options

    layer_set(*rgba_options, rmagick_image, image.path)

    image.reprocess!
    @rmagick_image = nil
    project.compile_with_place(self)

    write_redis_infos!
  end

  def rmagick_image
    @rmagick_image ||= Magick::Image.read(image.path)[0]
  end

  def image_url
    image.url
  end

  def x_y_for_project_uniq
    if project.places.where(x: x, y: y).present?
      errors.add(:title)
    end
  end

  def score
    @score ||= project.places_in_x * y + x
  end

  private
  # next method work with a Place#reprocess_image_perform
  def start_worker
    PlaceReprocessWorker.perform_async(id, [crop_width, crop_height,
                                            crop_x, crop_y],
                                            [r_component, g_component, b_component, a_component])

  end

  def end_worker
    Thread.new do
      sleep 2 until exists_in_redis?

      ActionCable.server.broadcast("places_for_project_#{project.slug}",
                                   action: 'new', place: redis_info,
                                   complete_info: "#{project.places.size}/#{project.total_places_count}") if exists_in_redis?
    end
  end

  def layer_set(r, g, b, a, img, out_path)
    r, g, b = [r, g, b].map(&:to_i)
    color_str = "rgba(#{r},#{g},#{b},#{a})"

    rect = Magick::Image.new(img.columns, img.rows) { self.background_color = color_str }
    img = img.composite(rect, 0, 0, Magick::MultiplyCompositeOp)
    img.write(out_path)
  end
end

# == Schema Information
#
# Table name: places
#
#  id                 :integer          not null, primary key
#  project_id         :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  description        :text
#  x                  :integer
#  y                  :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  title              :string
#
