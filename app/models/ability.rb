class Ability
  include CanCan::Ability

  def initialize(user, namespace)
    user ||= User.new
    case namespace
    when 'Manage'
      if user.admin?
        can [:index, :show, :new, :create, :edit, :update, :destroy], Project
      end
    else
      can [:index], Review
      can [:create], Review if user.persisted?
      can [:index, :show, :download, :download_counter], Project
      can [:edit], Project if user.admin?
      can [:show, :new, :create, :navigation], Place
    end
  end
end
