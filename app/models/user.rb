class User < ApplicationRecord
  extend UserOmniFinders

  has_many :reviews, dependent: :destroy
  has_attached_file :avatar, default_url: '/images/missing.png',
    styles: { medium: '120x120', small: '70x70', original: { processors: [:avatar_processor] } }

  validates_attachment_content_type :avatar,
    content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates :name, presence: true
  validates :password, presence: true, on: :create
  validates_format_of :email, with: /\A[^@\s]+@[^@\s]+\z/, allow_blank: false, unless: :with_provider?

  attr_accessor :crop_x, :crop_y, :crop_width, :crop_height

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  after_create :avatar_reprocess!

  def with_provider?
    provider.present?
  end

  def admin?
    role == 'admin'
  end

  def provider_name
    case provider
    when 'google_oauth2'
      'google'
    else
      provider
    end.capitalize
  end

  def avatar_reprocess!
    unless avatar_file_name
      img = Magick::Image.read("#{Rails.root}/public/images/for_cropps.jpg")[0]
      image_size = 120
      img.crop!(rand(1400 - image_size), rand(170 - image_size), image_size, image_size)
      temp = Tempfile.new
      img.write(temp.path)
      self.avatar = temp
      temp.close
      temp.unlink
    else
      avatar.reprocess!
    end

    update_attribute(:avatar_reserve_url, avatar.url)
  end

  def email_required?
    false
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  avatar_reserve_url     :string
#  provider               :string
#  uid                    :string
#  name                   :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role                   :string
#
