class Review < ApplicationRecord
  belongs_to :user

  scope :published, -> { where(state: 'published') }

  after_commit :processing, on: :create

  def self.per_page
    5
  end

  private
  def processing
    ReviewProcessingWorker.perform_in(1.minute, id)
  end
end

# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  state      :string
#  text       :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
