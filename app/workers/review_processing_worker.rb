class ReviewProcessingWorker
  include Sidekiq::Worker
  sidekiq_options retry: 5

  def perform(review_id)
    Review.find(review_id).update_attribute(:state, 'published')
  end
end
