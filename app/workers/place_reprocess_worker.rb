class PlaceReprocessWorker
  include Sidekiq::Worker
  sidekiq_options retry: 5

  sidekiq_retry_in do |count|
    2 + count
  end

  sidekiq_retries_exhausted do |msg, e|
    Sidekiq.logger.warn "Insert into DJQ!!\nclass:#{msg['class']}; args#{msg['args']}; \nmessage: #{msg['error_message']}"
  end

  def perform(place_id, cropp_options, rgb_options)
    Place.find(place_id).reprocess_image_perform(cropp_options, rgb_options)
  end
end
