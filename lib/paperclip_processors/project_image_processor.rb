module Paperclip
  class ProjectImageProcessor < Thumbnail
    def initialize file, options = {}, attachment = nil
      super

      return unless target.persisted?
      return if [target.places_in_y, target.places_in_x].all?

      @resized_width, @resized_height = target.width, target.height

      @cropped_width = target.width - target.width.modulo(target.place_height)
      @cropped_height = target.height - target.height.modulo(target.place_height)

      @current_geometry.width, @current_geometry.height = @cropped_width, @cropped_height

      in_x = @cropped_width / target.place_width
      in_y = @cropped_height / target.place_height


      target.update_columns(
                            width: @cropped_width,
                            height: @cropped_height,
                            places_in_y: in_y,
                            places_in_x: in_x
                           )
    end

    def transformation_command
      resize_command = [ '-resize', "#{@resized_width}x#{@resized_height}\\!" ] if [@resized_width, @resized_height].all?
      cropp_command = [ '-crop', "#{@cropped_width}x#{@cropped_height}+0+0", '+repage' ] if [@cropped_width, @cropped_height].all?
      [resize_command, cropp_command].compact + super
    end

    private
    def target
      @target ||= @attachment.instance
    end
  end
end
