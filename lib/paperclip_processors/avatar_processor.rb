module Paperclip
  class AvatarProcessor < Thumbnail
    def transformation_command
      command = super

      #p 'target.avatar.exists?'
      #p target.avatar.exists?
      #p '!target.avatar_reserve_url.present?'
      #p !target.avatar_reserve_url.present?
      #p '[:crop_height, :crop_width, :crop_x, :crop_y].map {|f| target.send(f).present? }.all?'
      #p [:crop_height, :crop_width, :crop_x, :crop_y].map {|f| target.send(f).present? }.all?

      # проверка на резервный урл нужна для создания юзера.
      # его нет при создании.
      # avatar.exists? не работал (on create) даже после INSERT INTO "users"
      if (target.avatar.exists? || !target.avatar_reserve_url.present?) &&
          [:crop_height, :crop_width, :crop_x, :crop_y].map {|f| target.send(f).present? }.all?

        crop_command = [ '-crop', "#{target.crop_width}x#{target.crop_height}+#{target.crop_x}+#{target.crop_y}" ]
        command += [crop_command].compact

      end

      command
    end

    private
    def target
      @target ||= @attachment.instance
    end
  end
end
