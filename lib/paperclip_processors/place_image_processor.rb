module Paperclip
  class PlaceImageProcessor < Thumbnail
    def transformation_command
      command = super

      if target.persisted? && [:crop_height, :crop_width, :crop_x, :crop_y].map {|f| target.send(f).present? }.all?
        crop_command = [ '-crop', "#{target.crop_width}x#{target.crop_height}+#{target.crop_x}+#{target.crop_y}" ]
        limit_size_command = [ '-resize', "#{Place.max_width}x#{Place.max_height}\\>", '+repage' ]
        command += [crop_command, limit_size_command]
      end

      command
    end

    private
    def target
      @target ||= @attachment.instance
    end
  end
end
