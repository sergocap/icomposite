module SidekiqMinitestSupport
  def after_teardown
    Sidekiq::Worker.clear_all
  end
end
