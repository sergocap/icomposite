module UserOmniFinders
  def find_for_vkontakte_oauth access_token
    user = User.find_by(provider: access_token.provider, uid: access_token.info.urls.Vkontakte)

    unless user
      user = User.new(provider: access_token.provider,
                      uid: access_token.info.urls.Vkontakte,
                      email: SecureRandom.uuid,
                      name: access_token.info.name)

      user.avatar = open(access_token.extra.raw_info.photo_200)
      user.save(validate: false)
    end

    user
  end

  def find_for_twitter_oauth access_token
    user = User.find_by(provider: access_token.provider, uid: access_token.info.urls.Twitter)

    unless user
      user = User.new(provider: access_token.provider,
                      uid: access_token.info.urls.Twitter,
                      email: SecureRandom.uuid,
                      name: access_token.info.name)

      user.avatar = open(access_token.info.image.remove('_normal'))
      user.save(validate: false)
    end

    user
  end

  def find_for_google_oauth2_oauth access_token
    user = User.find_by(provider: access_token.provider, uid: access_token.uid)

    unless user
      user = User.new(provider: access_token.provider,
                      uid: access_token.uid,
                      email: SecureRandom.uuid,
                      name: access_token.info.name)

      user.avatar = open(access_token.info.image)
      user.save(validate: false)
    end

    user
  end

  def find_for_yandex_oauth access_token
    user = User.find_by(provider: access_token.provider, uid: access_token.uid)

    unless user
      user = User.new(provider: access_token.provider,
                      uid: access_token.uid,
                      email: SecureRandom.uuid,
                      name: access_token.info.name)

      user.avatar = open("https://avatars.yandex.net/get-yapic/#{access_token.extra.raw_info.default_avatar_id}/islands-200")
      user.save(validate: false)
    end

    user
  end

  def find_for_facebook_oauth access_token
    user = User.find_by(provider: access_token.provider, uid: access_token.uid)

    unless user
      user = User.new(provider: access_token.provider,
                      uid: access_token.uid,
                      email: SecureRandom.uuid,
                      name: access_token.info.name)

      user.avatar = open("#{access_token.info.image}?type=large", allow_redirections: :safe)
      user.save(validate: false)
      cropp_if_not_square(user)
    end

    user
  end

  def cropp_if_not_square(user)
    width_av, height_av = FastImage.size(user.avatar.path)
    if width_av != height_av
      p 'width and height of avatar not equal'
      minimal_size = [width_av, height_av].min
      user.crop_height = minimal_size
      user.crop_width = minimal_size
      user.crop_x = 0
      user.crop_y = 0
      user.avatar.reprocess!
    else
      p 'width and height of avatar equal'
    end
  end
end
