module PlaceRedisObserver
  def self.included(base)
    base.extend(ClassMethods)
    base.class_eval do
      after_destroy :remove_redis_info
    end
  end

  module ClassMethods
    def redis_place(project_slug, x, y)
      Hashie::Mash.new $redis.hgetall in_redis_id(project_slug, x, y)
    end

    def next_redis_place(place)
      next_place_redis_id = $redis.zrangebyscore(key_for_project_orders(place.project_slug),
                                                 place.score, '+inf', limit: [1, 1])

      # first
      next_place_redis_id = $redis.zrangebyscore(key_for_project_orders(place.project_slug),
                                                 0, '+inf', limit: [0, 1]) unless next_place_redis_id.present?

      next_place_redis_id = in_redis_id(place.project_slug, place.x, place.y) unless next_place_redis_id.present?

      Hashie::Mash.new $redis.hgetall(next_place_redis_id)
    end

    def previous_redis_place(place)
      previous_place_redis_id = $redis.zrevrangebyscore(key_for_project_orders(place.project_slug),
                                                        place.score, '0', limit: [1, 1])

      # last
      previous_place_redis_id = $redis.zrevrangebyscore(key_for_project_orders(place.project_slug),
                                                        '+inf', '0', limit: [0, 1]) unless previous_place_redis_id.present?

      previous_place_redis_id = in_redis_id(place.project_slug, place.x, place.y) unless previous_place_redis_id.present?

      Hashie::Mash.new $redis.hgetall(previous_place_redis_id)
    end

    def in_redis_id(project_slug, x, y)
      "project_#{project_slug}:place_#{x}_#{y}"
    end

    def key_for_project_orders(project_slug)
      "project_orders:#{project_slug}"
    end
  end

  def place_info_hash
    {
      id: id, x: x, y: y,
      score: score,
      description: description,
      url: project_place_path(project.slug, self.id, x: x, y: y),
      project_id: project_id,
      title: title,
      project_slug: project.slug,
      image_url: image.url
    }
  end

  def create_scored_record
    $redis.zadd(Place.key_for_project_orders(self.project.slug), self.score, self.in_redis_id)
  end

  def set_redis_info(message = {})
    $redis.hmset(in_redis_id, *message.flatten)

    create_scored_record
  end

  def redis_info(field = nil)
    field ? $redis.hget(in_redis_id, field) : $redis.hgetall(in_redis_id)
  end

  def remove_redis_info
    $redis.del(in_redis_id)
  end

  def exists_in_redis?
    $redis.exists(in_redis_id)
  end

  def write_redis_infos!
    set_redis_info(place_info_hash)
  end

  def in_redis_id
    Place.in_redis_id(project.slug, x, y)
  end
end
